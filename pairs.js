
function pairs(obj) {
    let pArr = [];
    for(let keys in obj){
        let sArr = [];
        sArr.push(keys);
        sArr.push(obj[keys]);
        pArr.push(sArr);
    }
    return pArr ;
    // http://underscorejs.org/#pairs
}
module.exports = pairs ;