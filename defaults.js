
function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    let newObj = obj;

    for(let i in defaultProps){
        if(!obj[i]){
            newObj[i] = defaultProps[i]; 
        }
    }
    return newObj;
    
}
module.exports = defaults;