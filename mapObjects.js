

function mapObject(obj, cb) {
    let arr = [];
    for(let keys in obj){
        let v = cb(obj[keys]);
        arr.push(v);
    }
    return arr ;
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
}


module.exports = mapObject ;